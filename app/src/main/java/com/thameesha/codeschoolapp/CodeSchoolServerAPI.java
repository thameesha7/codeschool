package com.thameesha.codeschoolapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.ANResponse;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thameesha.codeschoolapp.DTO.DeviceDto;
import com.thameesha.codeschoolapp.DTO.ProjectDto;
import com.thameesha.codeschoolapp.DTO.UserDto;
import com.thameesha.codeschoolapp.DTO.UserRegisterDto;
import com.thameesha.codeschoolapp.ui.Device.DeviceListActivity;
import com.thameesha.codeschoolapp.ui.blockly.BlocklyWebActivity;
import com.thameesha.codeschoolapp.ui.dashboard.DashboardActivity;
import com.thameesha.codeschoolapp.ui.dialogBox.UserRegConfirm;
import com.thameesha.codeschoolapp.ui.userManagement.ConfirmUserRegistrationActivity;
import com.thameesha.codeschoolapp.ui.userManagement.LoginActivity;
import com.thameesha.codeschoolapp.ui.userManagement.RegisterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;

public class CodeSchoolServerAPI {

    public static final String serverAddress = "http://192.168.8.115/codeschoolserver/api/";
    public static final String serverMainAddress = "http://192.168.8.115/codeschoolserver/";
//    public static final String serverAddress = "http://192.168.8.115:11008/api/";
//    public static final String serverMainAddress = "http://192.168.8.115:11008/";
//    public static final String serverAddress = "http://codeschool.ddns.net/";
//    public static final String serverMainAddress = "http://codeschool.ddns.net/";
    //public static final String serverAddress = "http://localhost:11008/api/";
    private static SharedPreferences sp;
    public List<ProjectDto> projectList;

    public CodeSchoolServerAPI(SharedPreferences spd){
        sp = spd;
    }

    public void registerUser(UserRegisterDto userRegisterDto, final Context context){
        String url = serverAddress+"UserManage/register";
        projectList = new ArrayList<>();


        try {
            JSONObject user = new JSONObject();
            user.put("Id", "0");
            user.put("UserId", "0");
            user.put("Email", userRegisterDto.email);
            user.put("Password", userRegisterDto.password);
            user.put("FirstName", userRegisterDto.firstName);
            user.put("SecondName", userRegisterDto.secondName);
            user.put("SchoolId", userRegisterDto.schoolId);
            user.put("AccountType", userRegisterDto.accountType);
            user.put("Grade", userRegisterDto.grade);
            user.put("ParentName", userRegisterDto.parentName);
            user.put("ParentPhoneNo", userRegisterDto.parentPhoneNo);

            AndroidNetworking.post(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addJSONObjectBody(user)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .getAsObject(UserRegisterDto.class, new ParsedRequestListener<UserRegisterDto>() {
                        @Override
                        public void onResponse(UserRegisterDto userRegisterDto) {
                            if(userRegisterDto.id != null){
                                sp.edit().putString("userId",userRegisterDto.userId).apply();
                                sp.edit().putString("userEmail",userRegisterDto.email).apply();
                                sp.edit().putString("userPassword",userRegisterDto.password).apply();
                                if(userRegisterDto.status.equals("RequireConfirmation")){
                                    Intent intent = new Intent(context, ConfirmUserRegistrationActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("userId", userRegisterDto.userId); //Your id
                                    intent.putExtras(bundle); //Put your id to your next Intent
                                    context.startActivity(intent);
                                }else{
                                    Toast.makeText(context, "Error Occurred - "+userRegisterDto.status, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(context, "Error Occurred - "+anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void authenticateUser(String email, String password, final Context context){
        String url = serverAddress+"UserManage/signin";
        projectList = new ArrayList<>();

        try {
            JSONObject userAuth = new JSONObject();
            userAuth.put("Email", email);
            userAuth.put("Password", password);
            userAuth.put("RememberMe",false);

            AndroidNetworking.post(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addJSONObjectBody(userAuth)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .getAsObject(UserDto.class, new ParsedRequestListener<UserDto>() {
                        @Override
                        public void onResponse(UserDto userDto) {
                            if(userDto.id != null){
                                sp.edit().putString("userId",userDto.id).apply();
                                sp.edit().putString("userEmail",userDto.email).apply();
                                sp.edit().putString("userPassword",userDto.password).apply();
                                Intent intent = new Intent(context, DashboardActivity.class);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                            }else{
                                Toast.makeText(context, "Sorry! User Not Found", Toast.LENGTH_SHORT).show();
                                ProgressBar waiting = (ProgressBar) ((Activity) context).findViewById(R.id.loginLoading);
                                LinearLayout loginLayout = (LinearLayout) ((Activity) context).findViewById(R.id.loginLayout);
                                waiting.setVisibility(View.GONE);
                                loginLayout.setVisibility(View.VISIBLE);
                                Intent intent = new Intent(context, RegisterActivity.class);
                                context.startActivity(intent);
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(context, "Error Occurred - "+anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                            ProgressBar waiting = (ProgressBar) ((Activity) context).findViewById(R.id.loginLoading);
                            LinearLayout loginLayout = (LinearLayout) ((Activity) context).findViewById(R.id.loginLayout);
                            waiting.setVisibility(View.GONE);
                            loginLayout.setVisibility(View.VISIBLE);
                        }
                    });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(context, "Error Occurred - "+e.getMessage(), Toast.LENGTH_SHORT).show();
            ProgressBar waiting = (ProgressBar) ((Activity) context).findViewById(R.id.loginLoading);
            LinearLayout loginLayout = (LinearLayout) ((Activity) context).findViewById(R.id.loginLayout);
            waiting.setVisibility(View.GONE);
            loginLayout.setVisibility(View.VISIBLE);
        }
    }

    public List<ProjectDto> getProjectList(int projectType,String groupId){
        String userId = sp.getString("userId","");
        String userName = sp.getString("userEmail","");
        String password = sp.getString("userPassword","");
        final List<ProjectDto> projectDtos = new ArrayList<>();
        final Object[] obj = new Object[]{projectDtos};
        final ObjectMapper mapper = new ObjectMapper();

        String url = serverAddress+"project/getByProjectType/";//?projectType="+projectType;
//        if(projectType == 1){
//            url += "&userId="+userId;
//        }else{
//            url += "&groupId="+groupId;
//        }

        //String encodeBytes = Base64.getEncoder().encodeToString((userName + ":" + password).getBytes());

        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addQueryParameter("projectType","1")
                    .addQueryParameter("userId",userId)
                    .addQueryParameter("groupId","")
                    .setPriority(Priority.MEDIUM)
                    .setTag("projects")
                    .build()
                    .getAsObjectList(ProjectDto.class, new ParsedRequestListener<List<ProjectDto>>() {
                        @Override
                        public void onResponse(List<ProjectDto> response) {
                            projectDtos.addAll(response);
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("fff",anError.getErrorDetail());
                        }
                    });
//                        @Override
//                        public void onResponse(JSONArray response)
//                        {
//                            if(response != null){
//                                try {
//                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, ProjectDto.class);
//                                    List projs = mapper.readValue(String.valueOf(response), type);
//                                    obj[0] = projs;
//                                    projectDtos.addAll(projs);
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                        @Override
//                        public void onError(ANError error) {
//                            Log.e("ff",error.getErrorDetail());
//                        }
//                    });
//            if(obj[0] != null){
//                projectDtos.addAll((Collection<? extends ProjectDto>) obj[0]);
//            }
            return projectDtos;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public void createProject(ProjectDto projectDto, final Context context){
        String url = serverAddress+"Project/CreateProject";

        try {
            JSONObject project = new JSONObject();
            project.put("Id", "0");
            project.put("Name", projectDto.name);
            project.put("Description", projectDto.description);
            project.put("ProjectType", projectDto.projectType);
            project.put("CreateDate", projectDto.createDate);
            project.put("EndDate", projectDto.endDate);
            project.put("IsActive", projectDto.isActive);
            project.put("IsDelete", projectDto.isDelete);
            project.put("UserId", projectDto.userId);
            project.put("GroupId", projectDto.groupId);
            project.put("Status", "");
            project.put("ProjectUrl", "");

            AndroidNetworking.post(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addJSONObjectBody(project)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .getAsObject(ProjectDto.class, new ParsedRequestListener<ProjectDto>() {
                        @Override
                        public void onResponse(ProjectDto dto) {
                            if(dto.id != null){
                                sp.edit().putString("ProjectId",dto.id).apply();
                                if(dto.status.equals("Success")){
                                    Intent intent = new Intent(context, BlocklyWebActivity.class);
                                    intent.putExtra("ProjectId", dto.id); //Put your id to your next Intent
                                    intent.putExtra("ProjectUrl", dto.projectUrl);
                                    context.startActivity(intent);
                                    ((Activity)context).finish();
                                }else{
                                    Toast.makeText(context, "Error Occurred - "+dto.status, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(context, "Error Occurred - "+anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void setDefaultDevice(String deviceId , final Context context){
        String userId = sp.getString("userId","");
        String url = serverAddress+"device/SetDefault/";

        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addQueryParameter("userId",userId)
                    .addQueryParameter("deviceId",deviceId)
                    .setPriority(Priority.MEDIUM)
                    .setTag("devices")
                    .build()
                    .getAsObject(DeviceDto.class, new ParsedRequestListener<DeviceDto>() {
                        @Override
                        public void onResponse(DeviceDto response) {
                            if(response.status.equals("Success")){
                                Intent refresh = new Intent(context.getApplicationContext(), DeviceListActivity.class);
                                context.startActivity(refresh);
                                ((Activity)context).finish();
                            }else{
                                Toast.makeText(context, "Error Occurred - "+response.status, Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("err",anError.getErrorDetail());
                            Toast.makeText(context, "Error Occurred - "+anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void createDevice(DeviceDto deviceDto, final Context context){
        String url = serverAddress+"Device/CreateDevice";

        try {
            JSONObject device = new JSONObject();
            device.put("Id", deviceDto.id);
            device.put("UserId", deviceDto.userId);
            device.put("GroupId", deviceDto.groupId);
            device.put("deviceName", deviceDto.deviceName);
            device.put("deviceAccountType", deviceDto.deviceAccountType);
            device.put("deviceFamily", deviceDto.deviceFamily);
            device.put("boardType", deviceDto.boardType);
            device.put("availability", "Available");
            device.put("status", "Online");
            device.put("lastProject", deviceDto.lastProject);
            device.put("lastSoftwareVersion", deviceDto.lastSoftwareVersion);
            device.put("isDefault", false);
            device.put("wifiSsid", deviceDto.wifiSsid);
            device.put("wifiPassword", deviceDto.wifiPassword);

            AndroidNetworking.post(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addJSONObjectBody(device)
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .getAsObject(DeviceDto.class, new ParsedRequestListener<DeviceDto>() {
                        @Override
                        public void onResponse(DeviceDto dto) {
                            if(dto.id != null){
                                if(dto.status.equals("Success")){
                                    Intent refresh = new Intent(context.getApplicationContext(), DeviceListActivity.class);
                                    context.startActivity(refresh);
                                    ((Activity)context).finish();
                                }else{
                                    Toast.makeText(context, "Error Occurred - "+dto.status, Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(context, "Error Occurred - "+anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        }
                    });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
