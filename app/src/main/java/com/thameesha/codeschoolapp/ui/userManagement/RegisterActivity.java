package com.thameesha.codeschoolapp.ui.userManagement;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.SchoolDto;
import com.thameesha.codeschoolapp.DTO.UserRegisterDto;
import com.thameesha.codeschoolapp.R;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.*;

public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener {

    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    final ObjectMapper mapper = new ObjectMapper();
    private Spinner schoolSpinner;
    Validator validator;
    @NotEmpty(message = "We required your email")
    @Email(message = "Email is not in valid format")
    private EditText email;
    @Password(min = 8, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS,
            message = "Password must contain at least 8 characters Including UPPER/lower case, Special character and Numbers ")
    private EditText password;
    @NotEmpty(message = "We required your First name")
    private EditText firstname;
    @NotEmpty(message = "We required your Second name")
    private EditText secondname;
    private Spinner school;
    private Spinner grade;
    private EditText parentname;
    @Pattern(regex = "^(07[0-9]{1}-[0-9]{7}|)$", message = "Not Valid Format")
    private EditText parentphone;
    private TextView accountype;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        String userId = sp.getString("userId","");
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        String serverUrl = codeSchoolServerAPI.serverAddress;
        email = (EditText)findViewById(R.id.reg_email);
        password = (EditText)findViewById(R.id.reg_password);
        firstname = (EditText)findViewById(R.id.reg_firstname);
        secondname = (EditText)findViewById(R.id.reg_secondname);
        school = (Spinner)findViewById(R.id.reg_dropdown_school);
        grade = (Spinner)findViewById(R.id.reg_dropdown_grade);
        parentname = (EditText)findViewById(R.id.reg_parentname);
        parentphone = (EditText)findViewById(R.id.reg_parentphone);
        accountype = (TextView) findViewById(R.id.reg_txt_accountype);
        validator = new Validator(this);
        validator.setValidationListener(this);
        getSchools(serverUrl);
        getGrades();
        Switch accountType = (Switch) findViewById(R.id.reg_switch_usertype);
        final TextView accountTypeText = (TextView)findViewById(R.id.reg_txt_accountype);
        final LinearLayout studentOnlyLayer = (LinearLayout)findViewById(R.id.reg_studentOnly);
        accountType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    accountTypeText.setText("Teacher");
                    studentOnlyLayer.setVisibility(View.GONE);
                }else{
                    accountTypeText.setText("Student");
                    studentOnlyLayer.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void onRegister(View view) {
        validator.validate();
    }

    private void getSchools(final String serverUrl) {
        String url = serverUrl+"school/getallactive";
        schoolSpinner = (Spinner) findViewById(R.id.reg_dropdown_school);
        schoolSpinner.setEnabled(false);
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .setPriority(Priority.MEDIUM)
                    .setTag("schools")
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                try {
                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, SchoolDto.class);
                                    List schools = mapper.readValue(String.valueOf(response), type);

                                    ArrayList<SchoolDto> schoolList = new ArrayList<>();
                                    schoolList.addAll(schools);
                                    ArrayAdapter<SchoolDto> adapter =
                                            new ArrayAdapter<SchoolDto>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, schoolList);
                                    adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                                    schoolSpinner.setAdapter(adapter);
                                    schoolSpinner.setEnabled(true);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("school spinner",anError.getErrorDetail());
                        }
                    });

            schoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    ((TextView) selectedItemView).setTextColor(Color.WHITE);
                    SchoolDto sclDto = (SchoolDto) parentView.getItemAtPosition(position);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getGrades(){

        Spinner grade = (Spinner)findViewById(R.id.reg_dropdown_grade);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Grade 5");
        arrayList.add("Grade 6");
        arrayList.add("Grade 7");
        arrayList.add("Grade 8");
        arrayList.add("Grade 9");
        arrayList.add("Grade 10");
        arrayList.add("Grade 11");
        arrayList.add("Grade 12");
        arrayList.add("Grade 13");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList);
        grade.setAdapter(arrayAdapter);

        grade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private UserRegisterDto controlToObject(){
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        userRegisterDto.email = email.getText().toString();
        userRegisterDto.password = password.getText().toString();
        userRegisterDto.firstName = firstname.getText().toString();
        userRegisterDto.secondName = secondname.getText().toString();
        SchoolDto schoolDto = new SchoolDto();
        schoolDto = (SchoolDto)school.getSelectedItem();
        userRegisterDto.schoolId = schoolDto.id;
        userRegisterDto.accountType = accountype.getText().toString();
        userRegisterDto.grade = grade.getSelectedItem().toString();
        userRegisterDto.parentName = parentname.getText().toString();
        userRegisterDto.parentPhoneNo = parentphone.getText().toString();

        return userRegisterDto;
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, R.string.validationSuccessToast, Toast.LENGTH_SHORT).show();
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.reg_progressBar);
        ScrollView scrollView = (ScrollView)findViewById(R.id.reg_layout);
        UserRegisterDto userRegisterDto = new UserRegisterDto();
        userRegisterDto = controlToObject();
        scrollView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        codeSchoolServerAPI.registerUser(userRegisterDto,this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
