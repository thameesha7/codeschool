package com.thameesha.codeschoolapp.ui.userManagement;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.*;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.R;

import java.util.List;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener {

    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    Validator validator;
    @NotEmpty(message = "We required your email")
    @Email(message = "Email is not in valid format")
    EditText username;
    @Password(min = 8, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS,
            message = "Password must contain at least 8 characters Including UPPER/lower case, Special character and Numbers ")
    EditText password;
    ProgressBar waiting;
    LinearLayout loginLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        waiting = (ProgressBar) findViewById(R.id.loginLoading);
        loginLayout = (LinearLayout) findViewById(R.id.loginLayout);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    public void onLogin(View view){
        validator.validate();
    }


    public void onRegister(View view) {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, R.string.validationSuccessToast, Toast.LENGTH_SHORT).show();
        loginLayout.setVisibility(View.GONE);
        waiting.setVisibility(View.VISIBLE);
        codeSchoolServerAPI.authenticateUser(username.getText().toString(),password.getText().toString(),this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
}
