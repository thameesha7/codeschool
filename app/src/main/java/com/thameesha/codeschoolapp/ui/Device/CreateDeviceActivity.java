package com.thameesha.codeschoolapp.ui.Device;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.DeviceDto;
import com.thameesha.codeschoolapp.DTO.GroupDto;
import com.thameesha.codeschoolapp.DTO.SchoolDto;
import com.thameesha.codeschoolapp.DTO.UserRegisterDto;
import com.thameesha.codeschoolapp.R;

import org.json.JSONArray;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateDeviceActivity extends AppCompatActivity implements Validator.ValidationListener {

    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    final ObjectMapper mapper = new ObjectMapper();
    Validator validator;
    @NotEmpty(message = "We required your Device MAC Address")
    @Pattern(regex = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$",message = "Invalid MAC Addreess format eg:- d4:43:f5:33:53:3F")
    private EditText deviceId;
    @NotEmpty(message = "We required your Device name")
    private EditText deviceName;
    @NotEmpty(message = "We required your WIFI SSID")
    private EditText wifiSSID;
    @NotEmpty(message = "We required your WIFI Password")
    private EditText wifiPassword;
    private TextView accessType;
    private Spinner devFamily;
    private Spinner devBoard;
    private Spinner schoolSpinner;
    private Spinner groupSpinner;
    private LinearLayout accessLayout;
    Switch accessTypeSW;
    String serverUrl = codeSchoolServerAPI.serverAddress;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_device);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        userId = sp.getString("userId","");
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        deviceId = (EditText)findViewById(R.id.devMacAddress);
        deviceName = (EditText)findViewById(R.id.devDeviceName);
        wifiSSID = (EditText)findViewById(R.id.devWifiSsid);
        wifiPassword = (EditText)findViewById(R.id.devWifiPassword);
        accessType = (TextView)findViewById(R.id.dev_accessTypeName);
        accessTypeSW = (Switch) findViewById(R.id.swDeviceAccess);
        accessLayout = (LinearLayout)findViewById(R.id.devAccessLevelLayer);
        devFamily = (Spinner)findViewById(R.id.devDeviceFamily);
        devBoard = (Spinner)findViewById(R.id.devBoardType);
        schoolSpinner = (Spinner) findViewById(R.id.devAssignSchool);
        groupSpinner = (Spinner) findViewById(R.id.devAssignGroup);

        accessTypeSW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    accessType.setText("Public");
                    accessLayout.setVisibility(View.VISIBLE);
                }else{
                    accessType.setText("Private");
                    accessLayout.setVisibility(View.GONE);
                }
            }
        });

        validator = new Validator(this);
        validator.setValidationListener(this);
        getDeviceFamily();
        getBoardType();
        getSchools(serverUrl);

    }

    private void getDeviceFamily(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("NodeMCU");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList);
        devFamily.setAdapter(arrayAdapter);

        devFamily.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getBoardType(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("esp8266:esp8266:nodemcu");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList);
        devBoard.setAdapter(arrayAdapter);

        devBoard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ((TextView) view).setTextColor(Color.WHITE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getSchools(final String serverUrl) {
        String url = serverUrl+"school/getallactive";

        schoolSpinner.setEnabled(false);
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .setPriority(Priority.MEDIUM)
                    .setTag("schools")
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                try {
                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, SchoolDto.class);
                                    List schools = mapper.readValue(String.valueOf(response), type);

                                    ArrayList<SchoolDto> schoolList = new ArrayList<>();
                                    schoolList.addAll(schools);
                                    ArrayAdapter<SchoolDto> adapter =
                                            new ArrayAdapter<SchoolDto>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, schoolList);
                                    adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                                    schoolSpinner.setAdapter(adapter);
                                    schoolSpinner.setEnabled(true);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("school spinner",anError.getErrorDetail());
                        }
                    });

            schoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    ((TextView) selectedItemView).setTextColor(Color.WHITE);
                    SchoolDto sclDto = (SchoolDto) parentView.getItemAtPosition(position);
                    getGroups(serverUrl,sclDto.id);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getGroups(String serverUrl, String schoolId) {
        String url = serverUrl+"group/getBySchoolId";

        groupSpinner.setEnabled(false);
        if(schoolId != null){
            try {
                AndroidNetworking.get(url)
                        .addHeaders("Content-Type","application/json")
                        .addHeaders("Accept","application/json")
                        .addQueryParameter("schoolId",schoolId)
                        .setPriority(Priority.MEDIUM)
                        .setTag("groups")
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                if(response != null){
                                    try {
                                        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, GroupDto.class);
                                        List groups = mapper.readValue(String.valueOf(response), type);

                                        ArrayList<GroupDto> groupList = new ArrayList<>();
                                        groupList.addAll(groups);
                                        ArrayAdapter<GroupDto> adapter =
                                                new ArrayAdapter<GroupDto>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, groupList);
                                        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                                        groupSpinner.setAdapter(adapter);
                                        groupSpinner.setEnabled(true);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.e("group spinner",anError.getErrorDetail());
                            }
                        });

                groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        ((TextView) selectedItemView).setTextColor(Color.WHITE);
                        GroupDto grpDto = (GroupDto) parentView.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // your code here
                    }

                });

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void onNewDevice(View view) {
        validator.validate();
    }

    private DeviceDto controlToObject(){
        DeviceDto deviceDto = new DeviceDto();
        deviceDto.userId = userId;
        deviceDto.id = deviceId.getText().toString();
        deviceDto.deviceName = deviceName.getText().toString();
        deviceDto.wifiSsid = wifiSSID.getText().toString();
        deviceDto.wifiPassword = wifiPassword.getText().toString();
        deviceDto.deviceFamily = devFamily.getSelectedItem().toString();
        deviceDto.boardType = devBoard.getSelectedItem().toString();
        if(accessType.getText().toString().equals("Private")){
            deviceDto.deviceAccountType = 1;
        }else{
            GroupDto groupDto = new GroupDto();
            groupDto = (GroupDto)groupSpinner.getSelectedItem();
            deviceDto.groupId = groupDto.id;
            deviceDto.deviceAccountType = 2;
        }
         return deviceDto;
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, R.string.validationSuccessToast, Toast.LENGTH_SHORT).show();
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.dev_progressBar);
        ScrollView scrollView = (ScrollView)findViewById(R.id.dev_layout);
        DeviceDto deviceDto = new DeviceDto();
        deviceDto = controlToObject();
        scrollView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        codeSchoolServerAPI.createDevice(deviceDto,CreateDeviceActivity.this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CreateDeviceActivity.this, DeviceListActivity.class);
        CreateDeviceActivity.this.finish();
        startActivity(intent);
    }
}
