package com.thameesha.codeschoolapp.ui.Project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Selection;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.GroupDto;
import com.thameesha.codeschoolapp.DTO.ProjectDto;
import com.thameesha.codeschoolapp.DTO.SchoolDto;
import com.thameesha.codeschoolapp.R;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GroupProjectActivity extends AppCompatActivity {

    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    final ObjectMapper mapper = new ObjectMapper();
    private Spinner schoolSpinner;
    private Spinner groupSpinner;
    private ListView listView;
    private ProgressBar progBar;
    private Button btnSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_project);
        listView = findViewById(R.id.groupProjectList);
        progBar = findViewById(R.id.groupProjectProgress);
        listView.setVisibility(View.VISIBLE);
        progBar.setVisibility(View.GONE);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        String userId = sp.getString("userId","");
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        String serverUrl = codeSchoolServerAPI.serverAddress;
        getSchools(serverUrl);
    }

    private void getSchools(final String serverUrl) {
        String url = serverUrl+"school/getallactive";
        schoolSpinner = (Spinner) findViewById(R.id.schoolSelection);
        schoolSpinner.setEnabled(false);
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .setPriority(Priority.MEDIUM)
                    .setTag("schools")
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                try {
                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, SchoolDto.class);
                                    List schools = mapper.readValue(String.valueOf(response), type);

                                    ArrayList<SchoolDto> schoolList = new ArrayList<>();
                                    schoolList.addAll(schools);
                                    ArrayAdapter<SchoolDto> adapter =
                                            new ArrayAdapter<SchoolDto>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, schoolList);
                                    adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                                    schoolSpinner.setAdapter(adapter);
                                    schoolSpinner.setEnabled(true);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("school spinner",anError.getErrorDetail());
                        }
                    });

            schoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    SchoolDto sclDto = (SchoolDto) parentView.getItemAtPosition(position);
                    getGroups(serverUrl,sclDto.id);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getGroups(String serverUrl, String schoolId) {
        String url = serverUrl+"group/getBySchoolId";
        groupSpinner = (Spinner) findViewById(R.id.groupSelection);
        groupSpinner.setEnabled(false);
        if(schoolId != null){
            try {
                AndroidNetworking.get(url)
                        .addHeaders("Content-Type","application/json")
                        .addHeaders("Accept","application/json")
                        .addQueryParameter("schoolId",schoolId)
                        .setPriority(Priority.MEDIUM)
                        .setTag("groups")
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                if(response != null){
                                    try {
                                        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, GroupDto.class);
                                        List groups = mapper.readValue(String.valueOf(response), type);

                                        ArrayList<GroupDto> groupList = new ArrayList<>();
                                        groupList.addAll(groups);
                                        ArrayAdapter<GroupDto> adapter =
                                                new ArrayAdapter<GroupDto>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, groupList);
                                        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                                        groupSpinner.setAdapter(adapter);
                                        groupSpinner.setEnabled(true);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.e("group spinner",anError.getErrorDetail());
                            }
                        });

                groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        GroupDto grpDto = (GroupDto) parentView.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // your code here
                    }

                });

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    private void getProjectList(String serverUrl,String groupId){
        String url = serverUrl+"project/getByProjectType/";
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addQueryParameter("projectType","2")
                    .addQueryParameter("userId","")
                    .addQueryParameter("groupId",groupId)
                    .setPriority(Priority.MEDIUM)
                    .setTag("projects")
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                try {
                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, ProjectDto.class);
                                    List projs = mapper.readValue(String.valueOf(response), type);
                                    AdapterProject adbProject;
                                    ArrayList<ProjectDto> myListItems  = new ArrayList<ProjectDto>();
                                    myListItems.addAll(projs);
                                    adbProject= new AdapterProject (GroupProjectActivity.this, R.layout.list_template, myListItems);
                                    listView.setAdapter(adbProject);
                                    listView.setVisibility(View.VISIBLE);
                                    progBar.setVisibility(View.GONE);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("Group Project",anError.getErrorDetail());
                            Toast.makeText(GroupProjectActivity.this, "Error Occurred - "+anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                            listView.setVisibility(View.VISIBLE);
                            progBar.setVisibility(View.GONE);
                        }
                    });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            listView.setVisibility(View.VISIBLE);
            progBar.setVisibility(View.GONE);
            Toast.makeText(GroupProjectActivity.this, "Error Occurred - "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void Search(View view) {
        searchCall();
    }
    private void searchCall(){
        groupSpinner = (Spinner) findViewById(R.id.groupSelection);
        GroupDto grpDto = (GroupDto) groupSpinner.getSelectedItem();
        listView.setVisibility(View.GONE);
        progBar.setVisibility(View.VISIBLE);
        String serverUrl = codeSchoolServerAPI.serverAddress;
        getProjectList(serverUrl , grpDto.id);
    }

    public void createProject(View view) {
        Intent intent = new Intent(GroupProjectActivity.this, CreateGroupProject.class);
        startActivity(intent);
        GroupProjectActivity.this.finish();
    }
}
