package com.thameesha.codeschoolapp.ui.Project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.DeviceDto;
import com.thameesha.codeschoolapp.DTO.ProjectDto;
import com.thameesha.codeschoolapp.R;
import com.thameesha.codeschoolapp.ui.Device.CreateDeviceActivity;
import com.thameesha.codeschoolapp.ui.Device.DeviceListActivity;

import java.util.List;

public class CreateProjectActivity extends AppCompatActivity implements Validator.ValidationListener {

    String accountType;
    @NotEmpty(message = "We required your Project name")
    EditText projectName;
    @NotEmpty(message = "We required your Project description")
    EditText projectDescription;
    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    Validator validator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_project);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        validator = new Validator(this);
        validator.setValidationListener(this);
        accountType = "Student";
        projectName = (EditText)findViewById(R.id.project_name);
        projectDescription = (EditText)findViewById(R.id.project_description);
    }


    public void onNewProject(View view){
        validator.validate();
    }

    private ProjectDto controlToObject(){
        ProjectDto projectDto = new ProjectDto();
        projectDto.name = projectName.getText().toString();
        projectDto.description = projectDescription.getText().toString();
        projectDto.projectType = 1;//Student
        projectDto.userId = sp.getString("userId","");
        projectDto.isActive = true;
        projectDto.isDelete = false;
        return projectDto;
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, R.string.validationSuccessToast, Toast.LENGTH_SHORT).show();
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.myproj_progressBar);
        ScrollView scrollView = (ScrollView)findViewById(R.id.myproj_layout);
        ProjectDto projectDto = new ProjectDto();
        projectDto = controlToObject();
        scrollView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        codeSchoolServerAPI.createProject(projectDto, CreateProjectActivity.this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CreateProjectActivity.this, MyProjectActivity.class);
        CreateProjectActivity.this.finish();
        startActivity(intent);
    }
}
