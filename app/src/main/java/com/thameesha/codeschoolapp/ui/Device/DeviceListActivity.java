package com.thameesha.codeschoolapp.ui.Device;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.DeviceDto;
import com.thameesha.codeschoolapp.R;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DeviceListActivity extends AppCompatActivity {

    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    final ObjectMapper mapper = new ObjectMapper();
    ListView listView;
    ProgressBar progBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        listView = findViewById(R.id.devicesList);
        progBar = findViewById(R.id.deviceListProgress);
        listView.setVisibility(View.GONE);
        progBar.setVisibility(View.VISIBLE);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        String userId = sp.getString("userId","");
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        String url = codeSchoolServerAPI.serverAddress+"device/getAll/";
        getDeviceList(url,userId);
    }

    private void getDeviceList(String url, String userId) {
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addQueryParameter("userId",userId)
                    .setPriority(Priority.MEDIUM)
                    .setTag("devices")
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                try {
                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, DeviceDto.class);
                                    List devis = mapper.readValue(String.valueOf(response), type);
                                    AdapterDevice adbProject;
                                    ArrayList<DeviceDto> myListItems  = new ArrayList<DeviceDto>();
                                    myListItems.addAll(devis);
                                    ListView listView = findViewById(R.id.devicesList);
                                    ProgressBar progBar = findViewById(R.id.deviceListProgress);
                                    adbProject= new AdapterDevice (DeviceListActivity.this, R.layout.list_template2, myListItems);
                                    listView.setAdapter(adbProject);
                                    listView.setVisibility(View.VISIBLE);
                                    progBar.setVisibility(View.GONE);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Toast.makeText(DeviceListActivity.this, "Error Occurred - "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                    listView.setVisibility(View.GONE);
                                    progBar.setVisibility(View.VISIBLE);
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("Devices",anError.getErrorDetail());
                            Toast.makeText(DeviceListActivity.this, "Error Occurred - "+anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(DeviceListActivity.this, "Error Occurred - "+e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void createDevice(View view) {
        Intent intent = new Intent(DeviceListActivity.this, CreateDeviceActivity.class);
        DeviceListActivity.this.finish();
        startActivity(intent);
    }
}
