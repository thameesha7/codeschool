package com.thameesha.codeschoolapp.ui.userManagement;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.ProjectDto;
import com.thameesha.codeschoolapp.DTO.UserRegisterDto;
import com.thameesha.codeschoolapp.R;
import com.thameesha.codeschoolapp.ui.Project.AdapterProject;
import com.thameesha.codeschoolapp.ui.Project.MyProjectActivity;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ConfirmUserRegistrationActivity extends AppCompatActivity {

    private String userId = "";
    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_user_registration);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            userId = bundle.getString("userId");
        }

    }

    public void onConfirm(View view) {
        EditText codetext = (EditText)findViewById(R.id.confirmCode);
        String code = codetext.getText().toString();
        getConfirmation(userId,code);
    }

    private void getConfirmation(String userId,String code){
        String url = codeSchoolServerAPI.serverAddress+"UserManage/ConfirmEmail/";
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addQueryParameter("userId",userId)
                    .addQueryParameter("code",code)
                    .setPriority(Priority.IMMEDIATE)
                    .setTag("confirm")
                    .build()
                    .getAsObject(UserRegisterDto.class, new ParsedRequestListener<UserRegisterDto>() {
                        @Override
                        public void onResponse(UserRegisterDto userRegisterDto) {
                            if(userRegisterDto.id != null){
                                sp.edit().putString("userId",userRegisterDto.userId).apply();
                                sp.edit().putString("userEmail",userRegisterDto.email).apply();
                                sp.edit().putString("userPassword",userRegisterDto.password).apply();
                                if(userRegisterDto.status.equals("Success")){
                                    Intent intent = new Intent(ConfirmUserRegistrationActivity.this ,LoginActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("userId", userRegisterDto.userId);
                                    intent.putExtras(bundle);
                                    ConfirmUserRegistrationActivity.this.startActivity(intent);
                                }else{
                                    Toast.makeText(ConfirmUserRegistrationActivity.this, "Error Occurred", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Toast.makeText(ConfirmUserRegistrationActivity.this, anError.getErrorDetail(), Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
