package com.thameesha.codeschoolapp.ui.Project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.thameesha.codeschoolapp.DTO.ProjectDto;
import com.thameesha.codeschoolapp.R;
import com.thameesha.codeschoolapp.ui.blockly.BlocklyWebActivity;

import java.util.ArrayList;
import java.util.List;

public class AdapterProject extends ArrayAdapter<ProjectDto> {
    private Context mContext;
    int mResource;
    private List<ProjectDto> lProject;
    private static LayoutInflater inflater = null;

    public AdapterProject(@NonNull Context context, int resource, @NonNull List<ProjectDto> objects) {
        super(context, resource, objects);
        try {
            this.mContext = context;
            this.lProject = objects;
            this.mResource = resource;

            //inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {

        }
    }

    public int getCount() {
        return lProject.size();
    }

    public ProjectDto getItem(ProjectDto position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public TextView display_name;
        public TextView display_number;

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        final String id = getItem(position).id;
        String name = getItem(position).name;
        String desc = getItem(position).description;

//        ProjectDto pro = new ProjectDto(name,desc)
        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        final TextView tid = (TextView)convertView.findViewById(R.id.projectId);
        TextView tname = (TextView)convertView.findViewById(R.id.projectName);
        TextView tdesc = (TextView)convertView.findViewById(R.id.projectDescription);
        Button btnOpen = (Button)convertView.findViewById(R.id.projectDetails);

        tname.setText(name);
        tdesc.setText(desc);
        tid.setText(id);
        btnOpen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, BlocklyWebActivity.class);
                intent.putExtra("ProjectId", id);
                mContext.startActivity(intent);
            }
        });

        return convertView;

//        View vi = convertView;
//        final ViewHolder holder;
//        try {
//            if (convertView == null) {
//                vi = inflater.inflate(R.layout.activity_my_project, null);
//                holder = new ViewHolder();
//
//                holder.display_name = (TextView) vi.findViewById(R.id.projectName);
//                holder.display_number = (TextView) vi.findViewById(R.id.projectDescription);
//
//
//                vi.setTag(holder);
//            } else {
//                holder = (ViewHolder) vi.getTag();
//            }
//
//
//
//            holder.display_name.setText(lProject.get(position).name);
//            holder.display_number.setText(lProject.get(position).description);
//
//
//        } catch (Exception e) {
//
//
//        }
//        return vi;
    }
}
