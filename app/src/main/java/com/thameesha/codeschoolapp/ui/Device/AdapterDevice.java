package com.thameesha.codeschoolapp.ui.Device;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.DeviceDto;
import com.thameesha.codeschoolapp.R;
import com.thameesha.codeschoolapp.ui.blockly.BlocklyWebActivity;

import java.util.List;

public class AdapterDevice extends ArrayAdapter<DeviceDto> {
    private Context mContext;
    int mResource;
    private List<DeviceDto> lDevice;
    private static LayoutInflater inflater = null;
    CodeSchoolServerAPI codeSchoolServerAPI;

    public AdapterDevice(@NonNull Context context, int resource, @NonNull List<DeviceDto> objects) {
        super(context, resource, objects);
        try {
            this.mContext = context;
            this.lDevice = objects;
            this.mResource = resource;

            //inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        } catch (Exception e) {

        }
    }

    public int getCount() {
        return lDevice.size();
    }

    public DeviceDto getItem(DeviceDto position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public TextView display_name;
        public TextView display_number;

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        final String id = getItem(position).id;
        String mac = getItem(position).id;
        String name = getItem(position).deviceName;
        int accType = getItem(position).deviceAccountType;
        Boolean isDefult = getItem(position).isDefault;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView = inflater.inflate(mResource,parent,false);

        final TextView tid = (TextView)convertView.findViewById(R.id.deviceId);
        TextView tmac = (TextView)convertView.findViewById(R.id.macAddress);
        TextView tname = (TextView)convertView.findViewById(R.id.deviceName);
        TextView ttype = (TextView)convertView.findViewById(R.id.deviceAccessType);
        Button btnDefault = (Button)convertView.findViewById(R.id.deviceDefaultButton);

        if(isDefult){
            btnDefault.setBackgroundResource(R.drawable.green_button_style);
        }else {
            btnDefault.setBackgroundResource(R.drawable.red_button_style);
        }

        if(accType == 2){
            ttype.setText("G");
            ttype.setTextColor(Color.GRAY);
        }

        tmac.setText(mac);
        tname.setText(name);
        tid.setText(id);
        btnDefault.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                codeSchoolServerAPI.setDefaultDevice(id,mContext);
            }
        });

        return convertView;
    }
}
