package com.thameesha.codeschoolapp.ui.blockly;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.R;

import android.text.format.DateUtils;
import android.util.Log;
import android.webkit.*;

import java.io.File;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class BlocklyWebActivity extends AppCompatActivity {

    private WebView webView;
    Activity activity ;
    private ProgressDialog progDailog;
    SharedPreferences sp;
    String userId;
    String userName;
    String password;
    String projectId;
    String projectUrl;
    CodeSchoolServerAPI codeSchoolServerAPI;
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blockly_web);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        userId = sp.getString("userId","");
        userName = sp.getString("userEmail","");
        password = sp.getString("userPassword","");
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        projectId = getIntent().getExtras().getString("ProjectId");
        projectUrl = getIntent().getExtras().getString("ProjectUrl");

        activity = this;
        progDailog = ProgressDialog.show(activity, "Loading","Please wait...", true);
        progDailog.setCancelable(false);

        webView = (WebView) findViewById(R.id.blocklywebview);

        webView.reload();
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        this.deleteDatabase("webviewCache.db");
        this.deleteDatabase("webview.db");
        webView.clearCache(true);
        webView.clearHistory();
        webView.clearFormData();
        CookieManager.getInstance().removeAllCookies(null);
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                progDailog.show();
                view.loadUrl(url);

                return true;
            }
            @Override
            public void onPageFinished(WebView view, final String url) {

                progDailog.dismiss();
            }

            @Override
            public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
                handler.proceed(userName, password);
            }
        });

        Map<String, String> headers = new HashMap<String, String>();
        String encodeBytes = Base64.getEncoder().encodeToString((userName + ":" + password).getBytes());
        headers.put("Authorization","Basic "+encodeBytes);
        webView.loadUrl(CodeSchoolServerAPI.serverMainAddress+"Arduino/Blocky?projectId="+projectId,headers);


    }
}
