package com.thameesha.codeschoolapp.ui.Project;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.ProjectDto;
import com.thameesha.codeschoolapp.R;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyProjectActivity extends AppCompatActivity {

    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    final ObjectMapper mapper = new ObjectMapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_project);
        ListView listView = findViewById(R.id.myProjectList);
        ProgressBar progBar = findViewById(R.id.myProjectProgress);
        listView.setVisibility(View.GONE);
        progBar.setVisibility(View.VISIBLE);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        String userId = sp.getString("userId","");
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        String url = codeSchoolServerAPI.serverAddress+"project/getByProjectType/";
        getProjectList(url,userId);
    }

    public void createProject(View view) {
        Intent intent = new Intent(MyProjectActivity.this, CreateProjectActivity.class);
        startActivity(intent);
        MyProjectActivity.this.finish();
    }

    private void getProjectList(String url,String userId){
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .addQueryParameter("projectType","1")
                    .addQueryParameter("userId",userId)
                    .addQueryParameter("groupId","")
                    .setPriority(Priority.MEDIUM)
                    .setTag("projects")
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                try {
                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, ProjectDto.class);
                                    List projs = mapper.readValue(String.valueOf(response), type);
                                    AdapterProject adbProject;
                                    ArrayList<ProjectDto> myListItems  = new ArrayList<ProjectDto>();
                                    myListItems.addAll(projs);
                                    ListView listView = findViewById(R.id.myProjectList);
                                    ProgressBar progBar = findViewById(R.id.myProjectProgress);
                                    adbProject= new AdapterProject (MyProjectActivity.this, R.layout.list_template, myListItems);
                                    listView.setAdapter(adbProject);
                                    listView.setVisibility(View.VISIBLE);
                                    progBar.setVisibility(View.GONE);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("MyProject",anError.getErrorDetail());
                        }
                    });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
