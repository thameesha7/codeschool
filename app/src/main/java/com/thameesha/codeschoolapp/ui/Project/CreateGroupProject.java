package com.thameesha.codeschoolapp.ui.Project;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.thameesha.codeschoolapp.CodeSchoolServerAPI;
import com.thameesha.codeschoolapp.DTO.GroupDto;
import com.thameesha.codeschoolapp.DTO.ProjectDto;
import com.thameesha.codeschoolapp.DTO.SchoolDto;
import com.thameesha.codeschoolapp.R;

import org.json.JSONArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateGroupProject extends AppCompatActivity implements Validator.ValidationListener {

    SharedPreferences sp;
    CodeSchoolServerAPI codeSchoolServerAPI;
    final ObjectMapper mapper = new ObjectMapper();
    @NotEmpty(message = "We required your Project name")
    private EditText projectName;
    @NotEmpty(message = "We required your Project description")
    private EditText projectDescription;
    private Spinner schoolSpinner;
    private Spinner groupSpinner;
    Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_project);
        sp = getSharedPreferences("login",MODE_PRIVATE);
        String userId = sp.getString("userId","");
        codeSchoolServerAPI = new CodeSchoolServerAPI(sp);
        validator = new Validator(this);
        validator.setValidationListener(this);
        String serverUrl = codeSchoolServerAPI.serverAddress;
        projectName = (EditText)findViewById(R.id.group_project_name);
        projectDescription = (EditText)findViewById(R.id.group_project_description);
        getSchools(serverUrl);
    }

    private void getSchools(final String serverUrl) {
        String url = serverUrl+"school/getallactive";
        schoolSpinner = (Spinner) findViewById(R.id.group_schoolSelection);
        schoolSpinner.setEnabled(false);
        try {
            AndroidNetworking.get(url)
                    .addHeaders("Content-Type","application/json")
                    .addHeaders("Accept","application/json")
                    .setPriority(Priority.MEDIUM)
                    .setTag("schools")
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            if(response != null){
                                try {
                                    JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, SchoolDto.class);
                                    List schools = mapper.readValue(String.valueOf(response), type);

                                    ArrayList<SchoolDto> schoolList = new ArrayList<>();
                                    schoolList.addAll(schools);
                                    ArrayAdapter<SchoolDto> adapter =
                                            new ArrayAdapter<SchoolDto>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, schoolList);
                                    adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                                    schoolSpinner.setAdapter(adapter);
                                    schoolSpinner.setEnabled(true);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("school spinner",anError.getErrorDetail());
                        }
                    });

            schoolSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    ((TextView) selectedItemView).setTextColor(Color.WHITE);
                    SchoolDto sclDto = (SchoolDto) parentView.getItemAtPosition(position);
                    getGroups(serverUrl,sclDto.id);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void getGroups(String serverUrl, String schoolId) {
        String url = serverUrl+"group/getBySchoolId";
        groupSpinner = (Spinner) findViewById(R.id.group_groupSelection);
        groupSpinner.setEnabled(false);
        if(schoolId != null){
            try {
                AndroidNetworking.get(url)
                        .addHeaders("Content-Type","application/json")
                        .addHeaders("Accept","application/json")
                        .addQueryParameter("schoolId",schoolId)
                        .setPriority(Priority.MEDIUM)
                        .setTag("groups")
                        .build()
                        .getAsJSONArray(new JSONArrayRequestListener() {
                            @Override
                            public void onResponse(JSONArray response) {
                                if(response != null){
                                    try {
                                        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, GroupDto.class);
                                        List groups = mapper.readValue(String.valueOf(response), type);

                                        ArrayList<GroupDto> groupList = new ArrayList<>();
                                        groupList.addAll(groups);
                                        ArrayAdapter<GroupDto> adapter =
                                                new ArrayAdapter<GroupDto>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, groupList);
                                        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);
                                        groupSpinner.setAdapter(adapter);
                                        groupSpinner.setEnabled(true);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Log.e("group spinner",anError.getErrorDetail());
                            }
                        });

                groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        ((TextView) selectedItemView).setTextColor(Color.WHITE);
                        GroupDto grpDto = (GroupDto) parentView.getItemAtPosition(position);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // your code here
                    }

                });

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    public void onNewGroupProject(View view) {
        validator.validate();
    }

    private ProjectDto controlToObject(){
        ProjectDto projectDto = new ProjectDto();
        projectDto.name = projectName.getText().toString();
        projectDto.description = projectDescription.getText().toString();
        projectDto.projectType = 2;//Group
        groupSpinner = (Spinner) findViewById(R.id.group_groupSelection);
        GroupDto grpDto = (GroupDto) groupSpinner.getSelectedItem();
        projectDto.groupId = grpDto.id;
        projectDto.userId = sp.getString("userId","");
        projectDto.isActive = true;
        projectDto.isDelete = false;
        return projectDto;
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, R.string.validationSuccessToast, Toast.LENGTH_SHORT).show();
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.group_proj_progressBar);
        ScrollView scrollView = (ScrollView)findViewById(R.id.group_proj_layout);
        ProjectDto projectDto = new ProjectDto();
        projectDto = controlToObject();
        scrollView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        codeSchoolServerAPI.createProject(projectDto, CreateGroupProject.this);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(CreateGroupProject.this, GroupProjectActivity.class);
        CreateGroupProject.this.finish();
        startActivity(intent);
    }
}
