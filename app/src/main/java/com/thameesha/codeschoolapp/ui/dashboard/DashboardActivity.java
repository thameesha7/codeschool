package com.thameesha.codeschoolapp.ui.dashboard;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.thameesha.codeschoolapp.MainActivity;
import com.thameesha.codeschoolapp.R;
import com.thameesha.codeschoolapp.ui.Device.CreateDeviceActivity;
import com.thameesha.codeschoolapp.ui.Device.DeviceListActivity;
import com.thameesha.codeschoolapp.ui.Project.CreateProjectActivity;
import com.thameesha.codeschoolapp.ui.Project.GroupProjectActivity;
import com.thameesha.codeschoolapp.ui.Project.MyProjectActivity;
import com.thameesha.codeschoolapp.ui.blockly.BlocklyActivity;
import com.thameesha.codeschoolapp.ui.blockly.BlocklyWebActivity;
import com.thameesha.codeschoolapp.ui.userManagement.LoginActivity;

public class DashboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
    }

    public void openBlockly(View view) {
        Intent intent = new Intent(DashboardActivity.this, BlocklyWebActivity.class);
        startActivity(intent);
    }

    public void openMyProject(View view) {
        Intent intent = new Intent(DashboardActivity.this, MyProjectActivity.class);
        startActivity(intent);
    }

    public void openGroupProject(View view) {
        Intent intent = new Intent(DashboardActivity.this, GroupProjectActivity.class);
        startActivity(intent);
    }

    public void openDevice(View view) {
        Intent intent = new Intent(DashboardActivity.this, DeviceListActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
        DashboardActivity.this.finish();
        startActivity(intent);
    }
}
