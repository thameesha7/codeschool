package com.thameesha.codeschoolapp.DTO;

public class DeviceDto {
    public String id;
    public String userId;
    public String groupId;
    public String deviceName;
    public int deviceAccountType;
    public String deviceFamily;
    public String boardType;
    public String availability;
    public String status;
    public String lastProject;
    public String lastSoftwareVersion;
    public Boolean isDefault;
    public String wifiSsid;
    public String wifiPassword;
}
