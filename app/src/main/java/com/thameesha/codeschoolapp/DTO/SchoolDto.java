package com.thameesha.codeschoolapp.DTO;

public class SchoolDto {
    public String id;
    public String code;
    public String name;
    public int phoneNumber;
    public String address;
    public boolean isActive;
    public boolean isDelete;

    @Override
    public String toString() {
        return "("+code+") "+name;
    }
}
