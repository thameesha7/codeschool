package com.thameesha.codeschoolapp.DTO;

public class GroupDto {
    public String id;
    public String schoolId;
    public String groupName;
    public String groupDescription;
    public boolean isActive;
    public boolean isDelete;

    @Override
    public String toString() {
        return groupName;
    }
}
