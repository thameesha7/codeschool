package com.thameesha.codeschoolapp.DTO;

import org.json.JSONObject;

public class UserRegisterDto {
    public  String id;
    public  String userId;
    public  String email;
    public  String password;
    public  String firstName;
    public  String secondName;
    public  String schoolId;
    public  String accountType;
    public  String grade;
    public  String parentName;
    public  String parentPhoneNo;
    public  String status;
}
