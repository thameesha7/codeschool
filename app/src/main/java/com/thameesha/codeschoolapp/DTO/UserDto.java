package com.thameesha.codeschoolapp.DTO;

public final class UserDto {
    public  String id;
    public  String email;
    public  String password;
    public  String rememberMe;
}
