package com.thameesha.codeschoolapp.DTO;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ProjectDto {
    public String id;
    public String name;
    public String description;
    public int projectType;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS")
    public Date createDate;
    public Date endDate;
    public boolean isActive;
    public boolean isDelete;
    public String userId;
    public String groupId;
    public String status;
    public String projectUrl;
}
